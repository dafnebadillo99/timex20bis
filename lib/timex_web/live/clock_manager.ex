defmodule TimexWeb.ClockManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {_, now} = :calendar.local_time()

    Process.send_after(self(), :working, 1000)
    {:ok, %{ui_pid: ui, mode: Time, time: Time.from_erl!(now), st2: Idle}}
  end
  def handle_info(:"top-left", %{mode: Time} = state) do
    {:noreply, %{state | mode: SWatch}}
  end
  def handle_info(:"top-left", %{mode: SWatch, ui_pid: ui, time: time} = state) do
    GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    {:noreply, %{state | mode: Time}}
  end

  def handle_info(:working, %{ui_pid: ui, mode: mode, time: time} = state) do
    Process.send_after(self(), :working, 1000)
    time = Time.add(time, 1)
    if mode == Time do
      GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    end
    {:noreply, state |> Map.put(:time, time) }
  end

  def handle_info(:"bottom-right", %{st2: Idle} = state) do
    IO.puts("idle a waiting")
    Process.send_after(self(), :editing, 250)
    {:noreply,%{state | st2: Waiting}}
  end

  def handle_info(:"bottom-right", %{st2: Waiting} = state) do
    IO.puts("waiting a idle")
    {:noreply,%{state | st2: Idle}}
  end

  def handle_info(Editing, %{st2: Waiting, ui_pid: _ui} = state) do
    IO.puts("Waiting2editing")
    te2e = Process.send_after(self(), Editing2Editing, 250)
    :gproc.send({:p, :l, :ui_event}, :stop_clock)
    {:noreply, %{state | st2: Editing, count: 0, show: true, selection: Hour, te2e: te2e}}
  end

  def handle_info(Editing2Editing, %{ui_pid: ui, time: time, st2: Editing, count: count, show: show, selection: selection, te2e: te2e} = state) do
    Process.cancel_timer(te2e)
    #IO.puts(show)
    if count < 20 do
    te2e = Process.send_after(self(),Editing2Editing,250)
    count = count + 1
    show = !show
    GenServer.cast(ui, {:set_time_display, format(time, show, selection)})
    {:noreply, %{state | st2: Editing, count: count, show: show, te2e: te2e}}
      else
      GenServer.cast(ui, {:set_time_display, format(time, true, selection)})
      :gproc.send({:p, :l, :ui_event}, :resume_clock)
      {:noreply, %{state | st2: Idle, count: count, show: show, te2e: te2e}}
      end
    end

    def handle_info(:"bottom-right", %{ui_pid: ui, time: time, st2: Editing, selection: selection, te2e: te2e}=state) do
      Process.cancel_timer(te2e)
      te2e = Process.send_after(self(),Editing2Editing,250)
      selection = case selection do
      Hour -> Minute #if selection es igual a hour es minuto
      Minute -> Second #if
      _ -> Hour
      end
      GenServer.cast(ui, {:set_time_display, format(time, true, selection)})
      {:noreply, %{state | st2: Editing, count: 0, show: true, selection: selection, te2e: te2e}}
    end

    def handle_info(:"bottom-left", %{st2: Editing, time: time, selection: selection} = state) do
      Process.send_after(self(), :working, 1000)
      time = case selection do
      Hour -> Time.add(time, 360)
      Minute -> Time.add(time, 60)
      _ -> Time.add(time, 1)
      end
      {:noreply, %{state | st2: Editing, time: time, selection: selection}}
    end

    def format(time, show, selection) do
      _string =
        if show do
          "#{time.hour}:#{time.minute}:#{time.second}"
        else
          case selection do
            Hour -> ":#{time.minute}:#{time.second}"
            Minute -> "#{time.hour}: :#{time.second}"
            _ -> ":#{time.hour}:#{time.minute}:  "
          end
        end
      end

      def handle_info(:"bottom-left", %{ui_pid: _ui, mode: Time} = state) do
        timerStop = Process.send_after(self(), :edit_alarm, 250)
        {:noreply, %{state | mode: Waiting, timerStop: timerStop}}
      end

      def handle_info(:"bottom-left", %{ui_pid: _ui, mode: Waiting, timerStop: timerStop} = state) do
        Process.cancel_timer(timerStop)
        {:noreply, %{state | mode: Time}}
      end

      def handle_info(:edit_alarm, %{ui_pid: ui, mode: Waiting, selected: selected, show: show, alarm: alarm} = state) do
        Process.send_after(self(), :stop, 250)
        :gproc.send({:p, :l, :ui_event}, :edit_to_true)
        count = 0
        show = !show
        format(ui, alarm, show, selected)
        {:noreply, %{state | alarm: alarm, show: show, count: count, mode: AlarmEditing}}
      end

      def handle_info(:"bottom-left", %{ui_pid: ui, mode: AlarmEditing, selected: selected, alarm: alarm} = state) do
        count = 0
        alarm = increase(alarm, selected)
        GenServer.cast(ui, {:set_time_display, Time.truncate(alarm, :second) |> Time.to_string })
        {:noreply, %{state | count: count, alarm: alarm, mode: AlarmEditing}}
      end

      def handle_info(:"bottom-right", %{ui_pid: ui, mode: AlarmEditing, selected: selected, alarm: alarm} = state) do
        count = 0
        selected = change_selection(selected)
        GenServer.cast(ui, {:set_time_display, Time.truncate(alarm, :second) |> Time.to_string })
        {:noreply, %{state | count: count, selected: selected, mode: AlarmEditing}}
      end

      def handle_info(:stop, %{mode: AlarmEditing, count: count, alarm: alarm} = state) when count>=20 do
        :gproc.send({:p, :l, :ui_event}, :edit_to_false)
        {:noreply, %{state | selected: Hours,  alarm: alarm, mode: Time}}
      end

      def handle_info(:stop, %{ui_pid: ui, mode: AlarmEditing, selected: selected, show: show, count: count, alarm: alarm} = state) do
        Process.send_after(self(), :stop, 250)
        count = count + 1
        show = !show
        format(ui, alarm, show, selected)
        {:noreply, %{state | alarm: alarm, show: show, count: count, mode: AlarmEditing}}
      end

      def handle_info(:edit_to_false, state) do
        {:noreply, state}
      end

      def handle_info(:edit_to_true, state) do
        {:noreply, state}
      end

      def handle_info(:start_alarm, state) do
        {:noreply, state}
      end

      def handle_info(:"top-left", state) do
        {:noreply, state}
      end

      def handle_info(:"bottom-right", state) do
        {:noreply, state}
      end

      def handle_info(:"top-right", state) do
        {:noreply, state}
      end

      def handle_info(:"bottom-left", state) do
        {:noreply, state}
      end

      def increase(time, selected) do
        cond do
          selected==Hours -> Time.add(time, 3600)
          selected==Minutes -> Time.add(time, 60)
          selected==Seconds -> Time.add(time, 1)
          true -> time
        end
      end

      def change_selection(selected) do
        cond do
          selected==Hours   -> Minutes
          selected==Minutes -> Seconds
          selected==Seconds -> Hours
          true -> selected
        end
      end

      def format(ui, time, show, selected) do
        if show == true do
          GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
        else
          timeF = Time.truncate(time, :second) |> Time.to_string
          cond do
            selected == Hours   -> GenServer.cast(ui, {:set_time_display, "  " <> String.slice(timeF, 2..7)})
            selected == Minutes -> GenServer.cast(ui, {:set_time_display, String.slice(timeF, 0..2) <> "  " <> String.slice(timeF, 5..7)})
            selected == Seconds -> GenServer.cast(ui, {:set_time_display, String.slice(timeF, 0..5) <> "  "})
            true -> GenServer.cast(ui, {:set_time_display, timeF})
          end
        end
      end
end
